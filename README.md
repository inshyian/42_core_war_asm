It's an asm (assembler) part of 42_core_war project which is mine task to do in this project. Implemented using a finite-state machine.

42_core_war is a School 42 project. The purpose of this project is an implementation of the programming game “Core War”. It uses bots ("champions") wrote in a variation of Redcode language but with 16 instructions (not 19 as in original).
